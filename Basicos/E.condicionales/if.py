# La palabra clave if.

# Uno o más espacios en blanco.

# Una expresión(una pregunta o una respuesta)
# cuyo valor se interpretar únicamente en términos
#  de True (cuando su valor no sea cero) y
# False (cuando sea igual a cero).

# Unos dos puntos seguido de una nueva línea.

# Una instrucción con sangría o un conjunto de
# instrucciones(se requiere absolutamente al menos una instrucción)

# la sangría se puede lograr de dos maneras:
# insertando un número particular de espacios
# (la recomendación es usar cuatro espacios de sangría)
# , o usando el tabulador

# nota: si hay mas de una instrucción en la
# parte con sangría, la sangría debe ser la
# misma en todas las líneas

# aunque puede parecer lo mismo si se mezclan
# tabuladores con espacios, es importante que
# todas las sangrías sean exactamente iguales
# Python 3 no permite mezclar espacios y tabuladores
#  para la sangría.

# Si un determinado desarrollador de Python sin dormir se
# queda dormido cuando cuenta 120 ovejas, y el procedimiento
# de inducción del sueño se puede implementar como una función
# especial llamada dormirSoñar(), todo el código toma la siguiente forma:

def dormirSoñar(ov):
    print("Estoy soñando después de haber contado ", ov, ' ovejas', sep="@")


# def despertar(maxi, hi):
#     for i in range(2, maxi):
#     print("Prueba FOR ", hi)


contadorOvejas = int(input("No. de ovejas: "))

if contadorOvejas >= 120:  # evalua una expresión
    # se ejecuta si la expresión de prueba es Verdadera.
    dormirSoñar(contadorOvejas)
else:
    print("I can't got a dream")
