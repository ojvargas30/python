# print("¡Hola! Di algo")

# algo = input()

# print("Dijiste", algo, "¿en serio?. Que gracioso J<J<J<J<J< :D")


def menu():
    print("----------- Calculadora Solucalculator -----------")
    print("1. Suma")
    print("2. Resta")
    print("3. multiplicacion")
    print("4. Division")
    print("5. Módulo")
    # print("6. Raíz cuadrada")
    print("-----------------------------------")


def suma(a, b):
    return a + b


def resta(a, b):
    return a - b


def mul(a, b):
    return a * b


def div(a, b):
    if b == 0:
        return "ERROR -> DIVISIÓN ENTRE CERO NO ES CORRECTO"
    else:
        return a / b


def module(a, b):
    return a % b


# def root(a):
#     return float(a ** 0.5)


def default():
    return "Opción invalida"


def switch(case, a, b=None):  # switch despachado
    o = {
        1: suma(a, b),
        2: resta(a, b),
        3: mul(a, b),
        4: div(a, b),
        5: module(a, b)
        # 6: root(a)
    }
    return o.get(case, default())


menu()

op = int(input("Seleccione la operación a realizar con el número que lo identifica: "))
n1 = int(input("Valor de a: "))
n2 = int(input("Valor de b: "))
print("El resultado de la operación es: ", switch(op, n1, n2))

# if op == 6:
#     print("El resultado de la operación es: ", switch(op, n1))
# else:
#     n2 = int(input("Valor de b: "))
#     print("El resultado de la operación es: ", switch(op, n1, n2))
